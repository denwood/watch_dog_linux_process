#!/bin/bash 

for i in `ipcs -s | awk '/apache/ {print $2}'`; do (ipcrm -s $i); done
for i in `ipcs -m | awk '/apache/ {print $2}'`; do (ipcrm -m $i); done
systemctl start httpd
systemctl status httpd


exit 0