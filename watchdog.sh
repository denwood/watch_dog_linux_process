#=======================================#
# Author | Denwood                      #
#---------------------------------------#
# Mail   | KD.KouznetsovDenis@gmail.com #
#=======================================#

#!/bin/bash 

#---------------------------------------#
# Format date  
dt=$(date '+%d/%m/%Y %H:%M:%S');


# function action () {
    # /bin/sh $path
# }

# function get_path (full_path) {
#     path = full_path
# }

function read_config_file () {
    target_service=$(grep "service_name :" config.txt) 
    target_service = echo $target_service | cut -d: -f2
    return $target_service
}

# Log file 
function log() {
    if [ ! -f log ];then
        echo "$dt : $1" >> watchdog.log
    else 
        echo "$dt : $1" > watchdog.log        
    fi 
}

# Check if file & dataset present : if not then create a new 
function init_config_file() {
    flag=1
    file=config.txt
    # Check data set & if file exist 
    if ! grep -q 'service_name :' config.txt &> /dev/null; then
        log " service name not found  "
        flag=0
    fi
    if ! grep -q 'path :' config.txt &> /dev/null; then
        log " path not found "
        flag=0
    fi
    # Create config file 
    if [[ $flag = 0 ]];then
        rm ./config.txt &> /dev/null
        echo 'service_name : ' > ./config.txt
        echo 'path : ' >> ./config.txt
        log " create config file "
    fi
}

# > START 

init_config_file



exit 0 